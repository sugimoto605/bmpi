#使い方
UNAME = $(shell uname -s)
#ライブラリーたちの設定
MPI_CFLAGS = $(shell mpic++ --showme:compile)
MYSQL_CFLAGS = $(shell mysql_config --cflags)
MYSQL_LIBS = $(shell mysql_config --libs)
BOOST_LIBS = -lboost_coroutine\
	-lboost_date_time\
	-lboost_filesystem\
	-lboost_mpi\
	-lboost_program_options\
	-lboost_random\
	-lboost_regex\
	-lboost_thread\
	-lboost_timer\
	-lboost_serialization\
	-lboost_system\
	-lboost_wserialization



#現状MacではVTK7.1, 計算サーバーではVTK8.1
ifeq ($(UNAME),Darwin)
VTK_VERSION = 7.1
else
VTK_VERSION = 8.1
endif
VTK_LIBS =	-lvtkCommonCore-$(VTK_VERSION)\
			-lvtkCommonDataModel-$(VTK_VERSION)\
			-lvtkIOXML-$(VTK_VERSION)
MYSQLPP_LIBS=-lmysqlpp
LOG4CPP_LIBS=-llog4cplus
CRYPTO_LIBS=-lcrypto
CRYPTOPP_LIBS=-lcryptopp
#コンパイラーたちの設定
CC = echo
CXX = echo
CFLAGS =
CXXFLAGS =
LDFLAGS =
LOCAL_PATH = /usr/local
DEFINES = 
#macOS
ifeq ($(UNAME),Darwin)
LOCAL_PATH = /usr/local
CC = clang
CXX = clang++
CXXLD = clang++
VTK_INCLUDES = -I$(LOCAL_PATH)/include/vtk-$(VTK_VERSION)
CFLAGS = $(DEFINES) -Ofast -c $(MYSQL_CFLAGS) $(MPI_CFLAGS) $(VTK_INCLUDES)
CXXFLAGS = $(CFLAGS) -std=c++11
LDFLAGS = -Ofast -std=c++11
endif
#Linux
#	intel-setupスクリプトが$LOCAL_ROOTを設定している
ifeq ($(UNAME),Linux)
LOCAL_PATH = $(LOCAL_ROOT)
CC = icc
CXX = icpc
CXXLD = icpc
MPI_CXXLD = mpic++
VTK_INCLUDES = -I$(LOCAL_PATH)/include/vtk-$(VTK_VERSION)
CFLAGS = $(DEFINES) -Ofast -c $(MYSQL_CFLAGS) $(MPI_CFLAGS) $(VTK_INCLUDES) -Wno-deprecated -qopenmp
CXXFLAGS = -O0 -g -std=c++11 $(CFLAGS)
CXXFLAGS = $(CLAGS) -std=c++11
LDFLAGS = -Ofast -std=c++11 -qopenmp
endif
#汎用設定
OBJDIR = $(UNAME)
BOOST_CFLAGS = -I$(LOCAL_PATH)/include/boost
INCLUDE = -I$(LOCAL_PATH)/include
#LIBS = $(MYSQL_LIBS) -L$(LOCAL_PATH)/lib $(BOOST_LIBS) $(MYSQLPP_LIBS) $(LOG4CPP_LIBS) $(CRYPTO_LIBS) $(CRYPTOPP_LIBS) $(VTK_LIBS)
#----------------------------------
#デフォルトコンパイル
#$(OBJDIR)/%.o:EX1/%.cpp
#	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -c $<
#$(OBJDIR)/%:$(OBJDIR)/%.o
#	$(CXXLD) -o $@ $(LDFLAGS) $^ -L$(LOCAL_PATH)/lib $(LIBS)
#$(OBJDIR)/%.o:$(LIBSRC)/%.c
#	$(CXX) $(CFLAGS) $(INCLUDE) -o $@ -c $<
#$(OBJDIR)/%.o:$(LIBSRC)/%.cpp
#	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -c $<
#$(OBJDIR)/%.o:$(DSRC)/%.cpp
#	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -c $<
#$(SRC)/%.cpp:$(SRC)/%.hpp
#	@-touch $@
#----------------------------------
#ターゲットたち
all:clean EX1 EX2
	@echo "全てを作成しました"
clean:
	@if [ -d $(OBJDIR) ]; then rm -rf $(OBJDIR);fi
	@echo "ビルド用作業フォルダーをクリーンアップしました."
folder:
	@if [ ! -d $(OBJDIR) ]; then mkdir $(OBJDIR);fi
	@if [ ! -d $(OBJDIR)/EX1 ]; then mkdir $(OBJDIR)/EX1;fi
	@if [ ! -d $(OBJDIR)/EX2 ]; then mkdir $(OBJDIR)/EX2;fi
	@echo "ビルド用作業フォルダーは準備済みです."
EX1:folder $(OBJDIR)/EX1/main
	mpirun -np 2 $(OBJDIR)/EX1/main
$(OBJDIR)/EX1/main:$(OBJDIR)/EX1/main.o
	$(MPI_CXXLD) -o $@ $(LDFLAGS) $^ -L$(LOCAL_PATH)/lib $(LIBS)
	@echo "$@ created"
$(OBJDIR)/EX1/main.o:EX1/main.cpp
	$(CXX) $(CXXFLAGS) $(MPI_CFLAGS) $(INCLUDE) -o $@ -c $<
EX2:folder $(OBJDIR)/EX2/main
	mpirun -np 2 $(OBJDIR)/EX2/main
$(OBJDIR)/EX2/main:$(OBJDIR)/EX2/main.o
	$(MPI_CXXLD) -o $@ $(LDFLAGS) $(BOOST_LIBS) $^ -L$(LOCAL_PATH)/lib $(LIBS)
	@echo "$@ created"
$(OBJDIR)/EX2/main.o:EX2/main.cpp
	$(CXX) $(CXXFLAGS) $(MPI_CFLAGS) $(BOOST_CFLAGS) $(INCLUDE) -o $@ -c $<
