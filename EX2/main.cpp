//
//  main.cpp
//  EX2
//
//  Created by Hiroshi Sugimoto on 2019/02/15.
//  Copyright © 2019 Hiroshi Sugimoto. All rights reserved.
//

#include <iostream>
#include <vector>
#include <boost/mpi.hpp>
#include <boost/optional/optional_io.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
class Data {
private:
	friend class boost::serialization::access;
	template<class U> void serialize(U& ar,const unsigned int version){
		ar & id;
		ar & p;
		ar & T;
		ar & v;
		ar & SS;
		ar & Q;
	}
public:
	int id;
	double p;
	double T;
	double v[3];
	std::string SS;
	std::vector<double> Q;
	Data(){
		id=-1; p=T=2.; v[0]=v[1]=v[2]=-2;
		SS="Fuck";
		Q.resize(0);
	}
};
class Case {
private:
	boost::mpi::communicator& com;
public:
	std::vector<boost::mpi::request> requests;
	std::vector<double> V;
	std::vector<Data> DS;
	Data DR;
	double X;
	Case(boost::mpi::communicator& given_com):com(given_com){//初期化処理
		std::cout << "Hello, Boost.MPI CPU-" << com.rank() << std::endl;
		requests.clear();
		switch (com.rank()) {
			case 1:
				X=123.456;
				V.resize(12);
				for(int i=0;i<V.size();i++) V[i]=i;
				DS.resize(5);
				for(int i=0;i<DS.size();i++) {
					DS[i].id=i;
					DS[i].p=1+0.01*i;
					DS[i].T=1./(1+0.2*i);
					DS[i].v[0]=i;
					DS[i].v[1]=0.1*i;
					DS[i].v[2]=i*i;
					DS[i].Q.resize(5);
					DS[i].SS="Fukcing string-"+std::to_string(i);
					for(int j=0;j<DS[i].Q.size();j++) DS[i].Q[j]=i+j;
				}
				break;
			default:
				V.resize(12);
				DS.resize(5);
				break;
		}
	}
	void comm_start(){
		int this_tag=0x00FF;
		switch (com.rank()) {
			case 1:
//				requests.push_back(com.isend(0,this_tag , X));//OK
				std::cout << "SEND dize=" << V.size() << std::endl;
				requests.push_back(com.isend(0,this_tag , V.data(),V.size()));//OK
				break;
			default:
//				requests.push_back(com.irecv(1,this_tag , X));//OK
				requests.push_back(com.irecv(1,this_tag , V.data(), V.size()));//OK if V.size() is larger than sent data
//				requests.push_back(com.irecv(1,this_tag , V));//これが動けば楽なんだけど, まだだめ
				break;
		}
		std::cout << "comm_start() at CPU-" << com.rank() << " Requests=" << requests.size() << std::endl;
	}
	void comm_finish(){
		while (requests.size()>0) {
			auto DONE=boost::mpi::wait_any(requests.begin(), requests.end());
			auto ST=DONE.first;
			requests.erase(DONE.second);
			auto src_rank=ST.source();
			if (src_rank<0) continue;
			auto tag=ST.tag();
			boost::optional<int> count=ST.count<double>();
			std::cout << com.rank() << " got from " << src_rank << " [ " << tag << " ] ";
			std::cout << " count= " << count << " size= " << V.size() << std::endl;
		}
		std::cout << "comm_finish() at CPU-" << com.rank() << std::endl;
	}
	void data_start(){
		int this_tag=0x000F;
		switch (com.rank()) {
			case 1:
			{
				requests.push_back(com.isend(0,this_tag , DS[3]));//OK
				auto& D=DS[3];
				std::cout << "Sent Data.id= " << D.id << " Data.SS= " << D.SS << " Data.Q= ";
				for(auto &x: D.Q) std::cout << " " << x;
				std::cout << " Data.p= " << D.p << " Data.T= " << D.T ;
				std::cout << " Data.v= " << D.v[0] << " " << D.v[1] << " " <<  D.v[2] << std::endl;
			}
				break;
			default:
				requests.push_back(com.irecv(1,this_tag , DR));//OK if V.size() is larger than sent data
				auto& D=DR;
				std::cout << "Recv Data.id= " << D.id << " Data.SS= " << D.SS << " Data.Q= ";
				for(auto &x: D.Q) std::cout << " " << x;
				std::cout << " Data.p= " << D.p << " Data.T= " << D.T ;
				std::cout << " Data.v= " << D.v[0] << " " << D.v[1] << " " <<  D.v[2] << std::endl;
				break;
		}
		std::cout << "data_start() at CPU-" << com.rank() << " Requests=" << requests.size() << std::endl;
	}
	void data_finish(){
		while (requests.size()>0) {
			auto DONE=boost::mpi::wait_any(requests.begin(), requests.end());
			auto ST=DONE.first;
			requests.erase(DONE.second);
			auto src_rank=ST.source();
			if (src_rank<0) continue;
			auto tag=ST.tag();
			boost::optional<int> count=ST.count<Data>();
			std::cout << com.rank() << " got from " << src_rank << " [ " << tag << " ] ";
			std::cout << " count= " << count  << std::endl;
			auto& D=DR;
			std::cout << "Recv Data.id= " << D.id << " Data.SS= " << D.SS << " Data.Q= ";
			for(auto &x: D.Q) std::cout << " " << x;
			std::cout << " Data.p= " << D.p << " Data.T= " << D.T ;
			std::cout << " Data.v= " << D.v[0] << " " << D.v[1] << " " <<  D.v[2] << std::endl;
		}
		std::cout << "data_finish() at CPU-" << com.rank() << std::endl;
	}
	void vector_start(){
		int this_tag=0x001F;
		switch (com.rank()) {
			case 1:
			{
				requests.push_back(com.isend(0,this_tag , DS.data(), DS.size()));//OK
				for(auto &D:DS) {
					std::cout << "Sent Data.id= " << D.id << " Data.SS= " << D.SS << " Data.Q= ";
					for(auto &x: D.Q) std::cout << " " << x;
					std::cout << " Data.p= " << D.p << " Data.T= " << D.T ;
					std::cout << " Data.v= " << D.v[0] << " " << D.v[1] << " " <<  D.v[2] << std::endl;
				}
			}
				break;
			default:
				requests.push_back(com.irecv(1,this_tag , DS.data(), DS.size()));
				break;
		}
		std::cout << "vector_start() at CPU-" << com.rank() << " Requests=" << requests.size() << std::endl;
	}
	void vector_finish(){
		while (requests.size()>0) {
			auto DONE=boost::mpi::wait_any(requests.begin(), requests.end());
			auto ST=DONE.first;
			requests.erase(DONE.second);
			auto src_rank=ST.source();
			auto tag=ST.tag();
			boost::optional<int> count=ST.count<Data>();
			if (src_rank<0) continue;
			if (!count) continue;
			std::cout << com.rank() << " got from " << src_rank << " [ " << tag << " ] ";
			std::cout << " count= " << count  << std::endl;
			for(auto &D:DS) {
				std::cout << "Recv Data.id= " << D.id << " Data.SS= " << D.SS << " Data.Q= ";
				for(auto &x: D.Q) std::cout << " " << x;
				std::cout << " Data.p= " << D.p << " Data.T= " << D.T ;
				std::cout << " Data.v= " << D.v[0] << " " << D.v[1] << " " <<  D.v[2] << std::endl;
			}
		}
		std::cout << "vector_finish() at CPU-" << com.rank() << std::endl;
	}
	void broadcast_test(){
		int dat=com.rank()*com.rank();
		boost::mpi::broadcast(com, dat, 1);
		std::cout << com.rank() << " VALUE= " << dat << std::endl;
		std::vector<double> X;X.resize(5);
		switch (com.rank()) {
			case 1:
				break;
			default:
				for(int i=0;i<X.size();i++) X[i]=2.54-i;
				break;
		}
		boost::mpi::broadcast(com, X, 0);
		for(int i=0;i<X.size();i++) std::cout << "CPU-" << com.rank() << " X[" << i << "]= " << X[i] << std::endl;
		//std::vector<double*> PTR;PTR.resize(X.size());
		//boost::mpi::broadcast(com, PTR, 0);//ポインターは送れない
		//for(int i=0;i<X.size();i++) std::cout << "CPU-" << com.rank() << " PTR[" << i << "]= " << PTR[i] << std::endl;
	}
	void gather_test(){
		int dat=(com.rank()+1)*(com.rank()+1);
		std::vector<int> AR; AR.resize(2);
		boost::mpi::gather(com, dat, AR, 0);
		std::cout << com.rank() << " VALUE= " << dat << std::endl;
		std::cout << com.rank() << " AR[0]= " << AR[0] << std::endl;
		std::cout << com.rank() << " AR[1]= " << AR[1] << std::endl;
		
	}
	void reduce_test(){
		int dat=(com.rank()+1)*(com.rank()+1);
		int output;
		boost::mpi::reduce(com, dat, output, std::plus<int>(), 0);
		std::cout << com.rank() << " VALUE= " << dat << std::endl;
		std::cout << com.rank() << " output(+)= " << output << std::endl;
		boost::mpi::reduce(com, dat, output, boost::mpi::minimum<int>(), 0);
		std::cout << com.rank() << " output(min)= " << output << std::endl;
		boost::mpi::reduce(com, dat, output, boost::mpi::maximum<int>(), 0);
		std::cout << com.rank() << " output(max)= " << output << std::endl;
	}
};
int main(int argc, const char * argv[]) {
	boost::mpi::environment env;
	boost::mpi::communicator com;
	Case myCase(com);
//	myCase.comm_start();
//	myCase.comm_finish();
//	myCase.data_start();
//	myCase.data_finish();
//	myCase.vector_start();
//	myCase.vector_finish();
//	myCase.reduce_test();
	myCase.broadcast_test();
	com.barrier();
}
