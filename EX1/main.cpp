//Boost.MPIの使い方調査. ここではBoost.MPIの前に，OpenMPIライブラリーの動作を確認
#include <iostream>
#include <mpi.h>
int main(int argc, char * argv[]) {
	MPI_Init(&argc,&argv);
	std::cout << "Hello, OpenMPI" << std::endl;
	MPI_Finalize();
	return 0;
}
